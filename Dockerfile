FROM  openjdk:8-jre-alpine
ADD target/refresh-token-api-server-*.jar /root
ENTRYPOINT java -jar /root/refresh-token-api-server-*.jar --server.port=9095
EXPOSE 9095
