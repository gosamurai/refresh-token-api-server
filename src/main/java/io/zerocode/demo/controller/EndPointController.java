package io.zerocode.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EndPointController {

	@RequestMapping("/test")
	public String test() {
		return "{\"msg\": \"Hello Samurai\"}";
	}
}
