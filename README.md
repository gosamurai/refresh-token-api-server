1) Start authorization server   http://localhost:8080/oauth/token
2) Start Resource server http://localhost:9090/test

3) Get access and refresh token

URL = http://localhost:8080/oauth/token
METHOD = POST
Authorisation : Basic Auth :  zerocode-client:zerocode-secret

BODY
grant_type =  password
username = zerocode-user
password = zerocode-pass

Response :  {
                "access_token": "c79c01b6-983b-40d1-ac71-979a23db4a43",
                "token_type": "bearer",
                "refresh_token": "98227061-50a8-450a-8189-c0c28c7c2a0c",
                "expires_in": 43199,
                "scope": "read write"
            }
            

4) Get resource API
       URL =  http://localhost:9090/test   
       Authorisation = Bearer  c79c01b6-983b-40d1-ac71-979a23db4a43
       
Response = Hello World

5) Get Refresh Token from Access token

URL = http://localhost:8080/oauth/token

Authorisation : Basic Auth :  zerocode-client:zerocode-secret (Use the base64 format)

Body
grant_type = refresh_token
refresh_token = 98227061-50a8-450a-8189-c0c28c7c2a0c
            
